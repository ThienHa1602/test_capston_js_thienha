export let renderProductList = (productArr) => {
  let contentHTML = "";
  productArr.forEach((item) => {
    console.log("item: ", item);
    let contentDiv = `
    <div class="card col-3">
        <img class="card-img-top" src="${item.img}">
      <div class="card-body">
        <h4 class="card-title">${item.name}</h4>
        <h4 class="card-text">Price: ${item.price}</h4>
      </div>
      <button onclick="addToCart('${item.id}', '${item.img}','${item.price}')" class="btn btn-success">Add to cart</button>
    </div>
    `;
    contentHTML += contentDiv;
  });
  document.getElementById("productList").innerHTML = contentHTML;
};
export let valueType = (item) => {
  var value = document.getElementById("selectList").value;
  var list = [];
  for (var i = 0; i < item.length; i++) {
    if (item[i].type == value) {
      list.push(item[i]);
    } else if (value == "all") {
      list = item;
    }
  }
  return list;
};
export let renderCart = (cart) => {
  let contentHTML = "";
  cart.forEach((item) => {
    let contentTr = `
    <tr>
          <td>${item.id}</td>
          <td><img src=${item.img} /></td>
          <td>${item.price * item.soLuong}</td>
          <td>
            <button
            onclick="changeQuantity('${item.id}',-1)"
              
              class="btn btn-danger"
            >
              -
            </button>
            <strong class="mx-3">${item.soLuong}</strong>
            <button
              onclick="changeQuantity('${item.id}',1)"
              class="btn btn-primary"
            >
              +
            </button>
          </td>
          <td>
            <button
              onclick="deteleCartItem('${item.id}')"
              class="btn  border-danger text-danger"
            >
              Delete
            </button>
          </td>
        </tr>
    `;
    contentHTML += contentTr;
  });
  document.getElementById("tbodyCart").innerHTML = contentHTML;
};
export let onSuccess = (message) => {
  Toastify({
    text: message,
    duration: 3000,
    destination: "https://github.com/apvarun/toastify-js",
    newWindow: true,
    close: true,
    gravity: "top", // `top` or `bottom`
    position: "center", // `left`, `center` or `right`
    stopOnFocus: true, // Prevents dismissing of toast on hover
    style: {
      background: "linear-gradient(to right, #00b09b, #96c93d)",
    },
    onClick: function () {}, // Callback after click
  }).showToast();
};
